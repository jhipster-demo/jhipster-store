/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.storegw.web.rest.vm;
